/*Copyright (c) 2015-2016 wavemaker-com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker-com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker-com*/

package com.vcs_09dec_2.dynamohr126tabs;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;




/**
 * Loan generated by hbm2java
 */
@Entity
@Table(name="`loan`"
)
public class Loan  implements java.io.Serializable
 {


private Integer idLoan;
private String reason;
private Date loanDate;
private boolean collectedLoanWorksheet;
private boolean loanPaid;
private boolean externalLoan;
private BigDecimal fixedQuota;
private BigDecimal interestRate;
private Integer quantityPeriods;
private BigDecimal loanEntireValue;
private String observation;
private Integer modLocation;
private Integer modUser;
private Date modDate;
private Set<ListLoanDetailPayroll> listLoanDetailPayrolls = new HashSet<ListLoanDetailPayroll>(0);
private Currency currency;
private Employee employee;
private Set<PaymentLoanManual> paymentLoanManuals = new HashSet<PaymentLoanManual>(0);

    public Loan() {
    }



     @Id 

    

    @Column(name="`id_loan`", nullable=false)
    public Integer getIdLoan() {
        return this.idLoan;
    }
    
    public void setIdLoan(Integer idLoan) {
        this.idLoan = idLoan;
    }

    

    @Column(name="`reason`", nullable=false, length=50)
    public String getReason() {
        return this.reason;
    }
    
    public void setReason(String reason) {
        this.reason = reason;
    }

    @Temporal(TemporalType.DATE)

    @Column(name="`loan_date`", nullable=false, length=10)
    public Date getLoanDate() {
        return this.loanDate;
    }
    
    public void setLoanDate(Date loanDate) {
        this.loanDate = loanDate;
    }

    

    @Column(name="`collected_loan_worksheet`", nullable=false)
    public boolean isCollectedLoanWorksheet() {
        return this.collectedLoanWorksheet;
    }
    
    public void setCollectedLoanWorksheet(boolean collectedLoanWorksheet) {
        this.collectedLoanWorksheet = collectedLoanWorksheet;
    }

    

    @Column(name="`loan_paid`", nullable=false)
    public boolean isLoanPaid() {
        return this.loanPaid;
    }
    
    public void setLoanPaid(boolean loanPaid) {
        this.loanPaid = loanPaid;
    }

    

    @Column(name="`external_loan`", nullable=false)
    public boolean isExternalLoan() {
        return this.externalLoan;
    }
    
    public void setExternalLoan(boolean externalLoan) {
        this.externalLoan = externalLoan;
    }

    

    @Column(name="`fixed_quota`", nullable=false)
    public BigDecimal getFixedQuota() {
        return this.fixedQuota;
    }
    
    public void setFixedQuota(BigDecimal fixedQuota) {
        this.fixedQuota = fixedQuota;
    }

    

    @Column(name="`interest_rate`", nullable=false)
    public BigDecimal getInterestRate() {
        return this.interestRate;
    }
    
    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    

    @Column(name="`quantity_periods`", nullable=false)
    public Integer getQuantityPeriods() {
        return this.quantityPeriods;
    }
    
    public void setQuantityPeriods(Integer quantityPeriods) {
        this.quantityPeriods = quantityPeriods;
    }

    

    @Column(name="`loan_entire_value`", nullable=false)
    public BigDecimal getLoanEntireValue() {
        return this.loanEntireValue;
    }
    
    public void setLoanEntireValue(BigDecimal loanEntireValue) {
        this.loanEntireValue = loanEntireValue;
    }

    

    @Column(name="`observation`")
    public String getObservation() {
        return this.observation;
    }
    
    public void setObservation(String observation) {
        this.observation = observation;
    }

    

    @Column(name="`mod_location`", nullable=false)
    public Integer getModLocation() {
        return this.modLocation;
    }
    
    public void setModLocation(Integer modLocation) {
        this.modLocation = modLocation;
    }

    

    @Column(name="`mod_user`", nullable=false)
    public Integer getModUser() {
        return this.modUser;
    }
    
    public void setModUser(Integer modUser) {
        this.modUser = modUser;
    }

    @Temporal(TemporalType.TIMESTAMP)

    @Column(name="`mod_date`", nullable=false, length=19)
    public Date getModDate() {
        return this.modDate;
    }
    
    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="loan")
    public Set<ListLoanDetailPayroll> getListLoanDetailPayrolls() {
        return this.listLoanDetailPayrolls;
    }
    
    public void setListLoanDetailPayrolls(Set<ListLoanDetailPayroll> listLoanDetailPayrolls) {
        this.listLoanDetailPayrolls = listLoanDetailPayrolls;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="`fk_currency`", nullable=false)
    public Currency getCurrency() {
        return this.currency;
    }
    
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="`fk_employee`", nullable=false)
    public Employee getEmployee() {
        return this.employee;
    }
    
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="loan")
    public Set<PaymentLoanManual> getPaymentLoanManuals() {
        return this.paymentLoanManuals;
    }
    
    public void setPaymentLoanManuals(Set<PaymentLoanManual> paymentLoanManuals) {
        this.paymentLoanManuals = paymentLoanManuals;
    }



public boolean equals(Object o) {
         if (this == o)
         return true;
		 if ( (o == null ) )
		 return false;
		 if ( !(o instanceof Loan) )
		 return false;

		 Loan that = ( Loan ) o;

		 return ( (this.getIdLoan()==that.getIdLoan()) || ( this.getIdLoan()!=null && that.getIdLoan()!=null && this.getIdLoan().equals(that.getIdLoan()) ) );

   }

   public int hashCode() {
         int result = 17;

         result = 37 * result + ( getIdLoan() == null ? 0 : this.getIdLoan().hashCode() );

         return result;
     }


}

